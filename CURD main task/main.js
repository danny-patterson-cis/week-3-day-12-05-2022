let form = document.getElementById("form");
let textInput = document.getElementById("textInput");
let dateInput = document.getElementById("dateInput");
let textarea = document.getElementById("textarea");
let priority = document.getElementById("priority");
let msg1 = document.getElementById("msg1");
let msg2 = document.getElementById("msg2");
let msg3 = document.getElementById("msg3");
let tasks = document.getElementById("tasks");
let add = document.getElementById("add");
form.addEventListener("submit", (e) => {
    e.preventDefault();
    formValidation();
});

var t = document.getElementById("priority").selectedIndex;
var d = document.getElementsByTagName("option");

function caseChange() {
    var x = document.getElementById("textInput");
    x.value = x.value.toUpperCase();
}



let formValidation = () => {
    if (textInput.value === "" || dateInput.value === "" || textarea.value === "") {
        msg1.innerHTML = "Task Tittle can not be blank";
        msg2.innerHTML = "Please select a Date";
        msg3.innerHTML = "Please write something";
    }
    else {
        msg1.innerHTML = "";
        msg2.innerHTML = "";
        msg3.innerHTML = "";
        acceptData();
        add.setAttribute("data-bs-dismiss", "modal");
        add.click();
        (() => {
            add.setAttribute("data-bs-dismiss", "");
        })();
    }
};

let data = [{}];

let acceptData = () => {
    data.push({
        text: textInput.value,
        date: dateInput.value,
        description: textarea.value,
        priority:priority.value,
    })
    localStorage.setItem("data", JSON.stringify(data));

    createTask();
};

let createTask = () => {
    tasks.innerHTML = "";
    data.map((x, y) => {

        return (tasks.innerHTML += `
        <div id=${y}>
                    <span class="fw-bold">${x.text}</span>
                    <span class="small text-secondary">${x.date}</span>
                    <p>${x.description}</p>
                    <p>Priority:${x.priority}</p>
                    <span class="options">
                        <i onClick="editTask(this)" data-bs-toggle="modal" data-bs-target="form" class="fas fa-user-edit"></i>
                        <i onClick="deleteTask(this);createTask()" class="fas fa-trash"></i>
                    </span>
                    
                </div>
        `);
    })

    resetForm();
};
 
let deleteTask = (e) => {
    e.parentElement.parentElement.remove();
    data.splice(e.parentElement.parentElement.id, 1);
    localStorage.setItem("data", JSON.stringify(data))
};

let editTask = (e) => {
    let selectedTask = e.parentElement.parentElement;
    textInput.value = selectedTask.children[0].innerHTML;
    dateInput.value = selectedTask.children[1].innerHTML;
    textarea.value = selectedTask.children[2].innerHTML;
    priority.value = selectedTask.children[3].innerHTML;

    deleteTask(e);
};

let resetForm = () => {
    textInput.value = "";
    dateInput.value = "";
    textarea.value = "";

};
(() => {
    data = JSON.parse(localStorage.getItem("data")) || []
    createTask();
})();